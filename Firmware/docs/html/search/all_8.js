var searchData=
[
  ['i2cdrv_2ec',['I2Cdrv.c',['../_i2_cdrv_8c.html',1,'']]],
  ['i2cdrv_2eh',['I2Cdrv.h',['../_i2_cdrv_8h.html',1,'']]],
  ['i2cinit',['I2CInit',['../group__accel__cfg.html#ga138b5125051bf7b49ea731a3e3b5e669',1,'I2CInit():&#160;I2Cdrv.c'],['../group__accel__cfg.html#ga138b5125051bf7b49ea731a3e3b5e669',1,'I2CInit(void):&#160;I2Cdrv.c']]],
  ['i2creadreg',['I2CReadReg',['../group__accel__cfg.html#ga00c598d0e11e2c1561d42bcf28cd95eb',1,'I2CReadReg(uint8_t device_addr, uint8_t reg_addr, uint8_t *reg_val, uint8_t num_bytes):&#160;I2Cdrv.c'],['../_i2_cdrv_8h.html#a00c598d0e11e2c1561d42bcf28cd95eb',1,'I2CReadReg(uint8_t device_addr, uint8_t reg_addr, uint8_t *reg_val, uint8_t num_bytes):&#160;I2Cdrv.c']]],
  ['i2cwritereg',['I2CWriteReg',['../group__accel__cfg.html#ga3b4c55d613940bb28e0bf7fc1750f033',1,'I2CWriteReg(uint8_t device_addr, uint8_t reg_addr, uint8_t reg_val):&#160;I2Cdrv.c'],['../_i2_cdrv_8h.html#a3b4c55d613940bb28e0bf7fc1750f033',1,'I2CWriteReg(uint8_t device_addr, uint8_t reg_addr, uint8_t reg_val):&#160;I2Cdrv.c']]],
  ['irq_5fready',['irq_ready',['../bmd__i2c_8c.html#aa235da0676fc48fcf5834f37307075b1',1,'bmd_i2c.c']]],
  ['is_5fsrvc_5fchanged_5fcharact_5fpresent',['IS_SRVC_CHANGED_CHARACT_PRESENT',['../main_8c.html#ac0ad4bcfe8e9edf6794e9a796378978a',1,'main.c']]]
];
