var searchData=
[
  ['accelerometer_20configuration',['Accelerometer Configuration',['../group__accel__cfg.html',1,'']]],
  ['accel_5fchar_5fhandles',['accel_char_handles',['../structble__bmdeval__s.html#aa35d73891958e946076cd0aa7d1f966a',1,'ble_bmdeval_s']]],
  ['accel_5fconfig_5fdata_5fready',['Accel_Config_Data_Ready',['../group__accel__drv.html#ga42ce4c3bc2ad4bba924c680fd1e972eb',1,'Accel_Config_Data_Ready():&#160;bmd_i2c.c'],['../group__accel__drv.html#ga42ce4c3bc2ad4bba924c680fd1e972eb',1,'Accel_Config_Data_Ready(void):&#160;bmd_i2c.c']]],
  ['accel_5fconfig_5fdouble_5ftap',['Accel_Config_Double_Tap',['../group__accel__drv.html#ga2e217a8219c42c3e6fdea3b622745afd',1,'Accel_Config_Double_Tap():&#160;bmd_i2c.c'],['../group__accel__drv.html#ga2e217a8219c42c3e6fdea3b622745afd',1,'Accel_Config_Double_Tap(void):&#160;bmd_i2c.c']]],
  ['accel_5fconfig_5fmotion',['Accel_Config_Motion',['../group__accel__drv.html#gacc9bef077f8e0a43b90a67b3020e28d4',1,'Accel_Config_Motion():&#160;bmd_i2c.c'],['../group__accel__drv.html#gacc9bef077f8e0a43b90a67b3020e28d4',1,'Accel_Config_Motion(void):&#160;bmd_i2c.c']]],
  ['accel_5fconfig_5forientation',['Accel_Config_Orientation',['../group__accel__drv.html#gaa5f70d6f276469372134f09a51fabd0f',1,'Accel_Config_Orientation():&#160;bmd_i2c.c'],['../group__accel__drv.html#gaa5f70d6f276469372134f09a51fabd0f',1,'Accel_Config_Orientation(void):&#160;bmd_i2c.c']]],
  ['accel_5fconfig_5ftransient',['Accel_Config_Transient',['../group__accel__drv.html#ga4661c164c370e68063fd303707c133f6',1,'Accel_Config_Transient():&#160;bmd_i2c.c'],['../group__accel__drv.html#ga4661c164c370e68063fd303707c133f6',1,'Accel_Config_Transient(void):&#160;bmd_i2c.c']]],
  ['accel_5fdata_5fget',['Accel_Data_Get',['../group__accel__drv.html#gab36fa830b064b8c8e523fcb837d35d30',1,'Accel_Data_Get():&#160;bmd_i2c.c'],['../group__accel__drv.html#gab36fa830b064b8c8e523fcb837d35d30',1,'Accel_Data_Get(void):&#160;bmd_i2c.c']]],
  ['accel_5fdata_5ft',['accel_data_t',['../structaccel__data__t.html',1,'']]],
  ['accel_5fdeinit',['Accel_Deinit',['../group__accel__drv.html#gaff210f1947eaf0f6efca301193b1490e',1,'Accel_Deinit():&#160;bmd_i2c.c'],['../group__accel__drv.html#gaff210f1947eaf0f6efca301193b1490e',1,'Accel_Deinit(void):&#160;bmd_i2c.c']]],
  ['accelerometer_20driver',['Accelerometer Driver',['../group__accel__drv.html',1,'']]],
  ['accel_5finit',['Accel_Init',['../group__accel__drv.html#gad1fbcdfdf277e68625e4409f1942a8eb',1,'Accel_Init(ble_bmdeval_t *p_bmdeval):&#160;bmd_i2c.c'],['../group__accel__drv.html#gad1fbcdfdf277e68625e4409f1942a8eb',1,'Accel_Init(ble_bmdeval_t *p_bmdeval):&#160;bmd_i2c.c']]],
  ['accel_5firq_5fpin',['ACCEL_IRQ_PIN',['../bmd__i2c_8c.html#a76d00ed8f468da73b6553cef6cbc390a',1,'bmd_i2c.c']]],
  ['accel_5fmotion_5fstart',['ACCEL_MOTION_START',['../group___control___point___commands.html#ga4a6e8aeb53af7a8a18551cfbf84dfb42',1,'main.c']]],
  ['accel_5forientation_5fstart',['ACCEL_ORIENTATION_START',['../group___control___point___commands.html#gadbb5f32767c764147bb879e986450f3b',1,'main.c']]],
  ['accel_5fpulse_5fstart',['ACCEL_PULSE_START',['../group___control___point___commands.html#ga067e4c87c39d909a78224a29c51ee0d9',1,'main.c']]],
  ['accel_5fstop',['ACCEL_STOP',['../group___control___point___commands.html#ga26a3dcf4199973b63e8f53e6f6b812a4',1,'main.c']]],
  ['accel_5fstream_5fstart',['ACCEL_STREAM_START',['../group___control___point___commands.html#ga0f6365ae24057ba4c5173982d26248c0',1,'main.c']]],
  ['accel_5fstreaming_5fhandler',['Accel_Streaming_Handler',['../group__accel__drv.html#ga5c73aff9f42a0c07094d963604e74b62',1,'Accel_Streaming_Handler(void):&#160;bmd_i2c.c'],['../group__accel__drv.html#ga5c73aff9f42a0c07094d963604e74b62',1,'Accel_Streaming_Handler(void):&#160;bmd_i2c.c']]],
  ['accel_5fstreaming_5fmode',['Accel_Streaming_Mode',['../group__accel__drv.html#gae37f449b0e511672085b0614ace357e4',1,'Accel_Streaming_Mode(bool on_off):&#160;bmd_i2c.c'],['../group__accel__drv.html#gae37f449b0e511672085b0614ace357e4',1,'Accel_Streaming_Mode(bool on_off):&#160;bmd_i2c.c']]],
  ['accel_5ftransient_5fstart',['ACCEL_TRANSIENT_START',['../group___control___point___commands.html#ga77d92ef8001ba71ea89a2e07a8d00acd',1,'main.c']]],
  ['adc_5fchar_5fhandles',['adc_char_handles',['../structble__bmdeval__s.html#a90568b91d90c391469f200073265e83c',1,'ble_bmdeval_s']]],
  ['adc_5fdeinit',['ADC_Deinit',['../group__adc__module.html#ga06349d9b24eb80fce7b67603066fb78f',1,'ADC_Deinit(void):&#160;bmd_adc.c'],['../group__adc__module.html#ga06349d9b24eb80fce7b67603066fb78f',1,'ADC_Deinit(void):&#160;bmd_adc.c']]],
  ['adc_5finit',['ADC_Init',['../group__adc__module.html#ga5dd05ac9d600d39097342b2721f2452a',1,'ADC_Init(ble_bmdeval_t *p_bmdeval):&#160;bmd_adc.c'],['../group__adc__module.html#ga5dd05ac9d600d39097342b2721f2452a',1,'ADC_Init(ble_bmdeval_t *p_bmdeval):&#160;bmd_adc.c']]],
  ['adc_5firqhandler',['ADC_IRQHandler',['../group__adc__module.html#ga06406eadf297fa89a6eaf9586b227a69',1,'bmd_adc.c']]],
  ['adc_20module',['ADC Module',['../group__adc__module.html',1,'']]],
  ['adc_5fresult',['adc_result',['../group__adc__module.html#ga5aa050172a815a749f2fcd329ebe2321',1,'bmd_adc.c']]],
  ['adc_5fsample',['adc_sample',['../group__adc__module.html#ga320921bc25468395511e83824082ff19',1,'bmd_adc.c']]],
  ['adc_5fstream_5fstart',['ADC_STREAM_START',['../group___control___point___commands.html#ga19ebe44dbc6c1a4f9500cd1f1c312822',1,'main.c']]],
  ['adc_5fstream_5fstop',['ADC_STREAM_STOP',['../group___control___point___commands.html#ga8cb9f27ba42c893aff13b8d1dde92dab',1,'main.c']]],
  ['adc_5fvalue_5fget',['ADC_Value_Get',['../group__adc__module.html#gaa5518537d1cdf6ae947ba3b060fb9ed2',1,'ADC_Value_Get(void):&#160;bmd_adc.c'],['../group__adc__module.html#gaa5518537d1cdf6ae947ba3b060fb9ed2',1,'ADC_Value_Get(void):&#160;bmd_adc.c']]],
  ['adc_5fvalue_5fstart',['ADC_Value_Start',['../group__adc__module.html#gacd1192580d6eb70ee15659cd14a9be30',1,'ADC_Value_Start(void):&#160;bmd_adc.c'],['../group__adc__module.html#gacd1192580d6eb70ee15659cd14a9be30',1,'ADC_Value_Start(void):&#160;bmd_adc.c']]],
  ['app_5fadv_5finterval',['APP_ADV_INTERVAL',['../main_8c.html#adde0b932e57e128e4cd81c2dca47cfe3',1,'main.c']]],
  ['app_5fadv_5ftimeout_5fin_5fseconds',['APP_ADV_TIMEOUT_IN_SECONDS',['../main_8c.html#af58371bad8e1be8e2323df60379ed2df',1,'main.c']]],
  ['app_5fgpiote_2eh',['app_gpiote.h',['../app__gpiote_8h.html',1,'']]],
  ['app_5fgpiote_5fbuf_5fsize',['APP_GPIOTE_BUF_SIZE',['../group__app__gpiote.html#ga1776902d69e12c277659e609fcbf2fe5',1,'app_gpiote.h']]],
  ['app_5fgpiote_5fdisable_5finterrupts',['app_gpiote_disable_interrupts',['../group__app__gpiote.html#ga81f2b093519e072e7e875d3752948778',1,'app_gpiote.h']]],
  ['app_5fgpiote_5fenable_5finterrupts',['app_gpiote_enable_interrupts',['../group__app__gpiote.html#ga328efc6349aeb671173d4934008996f3',1,'app_gpiote.h']]],
  ['app_5fgpiote_5fend_5firq_5fevent_5fhandler_5fregister',['app_gpiote_end_irq_event_handler_register',['../group__app__gpiote.html#ga156c09d369b6f4169640f0c5f3b58d8c',1,'app_gpiote.h']]],
  ['app_5fgpiote_5fend_5firq_5fevent_5fhandler_5funregister',['app_gpiote_end_irq_event_handler_unregister',['../group__app__gpiote.html#gab19aa3044fd57a44ac309b2275958580',1,'app_gpiote.h']]],
  ['app_5fgpiote_5fevent_5fhandler_5ft',['app_gpiote_event_handler_t',['../group__app__gpiote.html#ga24ba6368f889d81193a7a983fae6cabc',1,'app_gpiote.h']]],
  ['app_5fgpiote_5finit',['APP_GPIOTE_INIT',['../group__app__gpiote.html#gaed385f30da9f44fb5bf4591eef4d27d9',1,'APP_GPIOTE_INIT():&#160;app_gpiote.h'],['../group__app__gpiote.html#ga573b71494c38cc4c32b209a13708c78d',1,'app_gpiote_init(uint8_t max_users, void *p_buffer):&#160;app_gpiote.c'],['../group__app__gpiote.html#ga573b71494c38cc4c32b209a13708c78d',1,'app_gpiote_init(uint8_t max_users, void *p_buffer):&#160;app_gpiote.c']]],
  ['app_5fgpiote_5finput_5fevent_5fhandler_5fregister',['app_gpiote_input_event_handler_register',['../group__app__gpiote.html#gadb3478898eb54905aa38d7ddf731d480',1,'app_gpiote.h']]],
  ['app_5fgpiote_5finput_5fevent_5fhandler_5ft',['app_gpiote_input_event_handler_t',['../group__app__gpiote.html#ga3090bc91a0b9ae665801e797a6122493',1,'app_gpiote.h']]],
  ['app_5fgpiote_5finput_5fevent_5fhandler_5funregister',['app_gpiote_input_event_handler_unregister',['../group__app__gpiote.html#ga87444fbfe469eb73229d43fdf8cd61fc',1,'app_gpiote.h']]],
  ['app_5fgpiote_5fmax_5fusers',['APP_GPIOTE_MAX_USERS',['../main_8c.html#a5ed22f8f4e1728b3e52e861b87ca66c4',1,'main.c']]],
  ['app_5fgpiote_5fpins_5fstate_5fget',['app_gpiote_pins_state_get',['../group__app__gpiote.html#ga5eaa7bc1dcd21b11a809dbd13b714d75',1,'app_gpiote_pins_state_get(app_gpiote_user_id_t user_id, uint32_t *p_pins):&#160;app_gpiote.c'],['../group__app__gpiote.html#ga5eaa7bc1dcd21b11a809dbd13b714d75',1,'app_gpiote_pins_state_get(app_gpiote_user_id_t user_id, uint32_t *p_pins):&#160;app_gpiote.c']]],
  ['app_5fgpiote_5fuser_5fdisable',['app_gpiote_user_disable',['../group__app__gpiote.html#ga486576266f08b96279e514f080911350',1,'app_gpiote_user_disable(app_gpiote_user_id_t user_id):&#160;app_gpiote.c'],['../group__app__gpiote.html#ga486576266f08b96279e514f080911350',1,'app_gpiote_user_disable(app_gpiote_user_id_t user_id):&#160;app_gpiote.c']]],
  ['app_5fgpiote_5fuser_5fenable',['app_gpiote_user_enable',['../group__app__gpiote.html#ga50622a63550c442d6ad8a8a0d34b166d',1,'app_gpiote_user_enable(app_gpiote_user_id_t user_id):&#160;app_gpiote.c'],['../group__app__gpiote.html#ga50622a63550c442d6ad8a8a0d34b166d',1,'app_gpiote_user_enable(app_gpiote_user_id_t user_id):&#160;app_gpiote.c']]],
  ['app_5fgpiote_5fuser_5fregister',['app_gpiote_user_register',['../group__app__gpiote.html#gaa0fe3334c8940d86fc48af00ccb8e0ed',1,'app_gpiote_user_register(app_gpiote_user_id_t *p_user_id, uint32_t pins_low_to_high_mask, uint32_t pins_high_to_low_mask, app_gpiote_event_handler_t event_handler):&#160;app_gpiote.c'],['../group__app__gpiote.html#gaa0fe3334c8940d86fc48af00ccb8e0ed',1,'app_gpiote_user_register(app_gpiote_user_id_t *p_user_id, uint32_t pins_low_to_high_mask, uint32_t pins_high_to_low_mask, app_gpiote_event_handler_t event_handler):&#160;app_gpiote.c']]],
  ['app_5ftimer_5fmax_5ftimers',['APP_TIMER_MAX_TIMERS',['../main_8c.html#ad5accf4f59399fd2dd980e1569deac3e',1,'main.c']]],
  ['app_5ftimer_5fop_5fqueue_5fsize',['APP_TIMER_OP_QUEUE_SIZE',['../main_8c.html#a756f526e607f350705dc7a2e05027cf2',1,'main.c']]],
  ['app_5ftimer_5fprescaler',['APP_TIMER_PRESCALER',['../main_8c.html#a7bbff5bb0ca047b109e70a831f08e217',1,'main.c']]],
  ['assert_5fnrf_5fcallback',['assert_nrf_callback',['../main_8c.html#a4df3b6dd09cac7a9c1705e80fcb735cb',1,'main.c']]]
];
