var searchData=
[
  ['next_5fconn_5fparams_5fupdate_5fdelay',['NEXT_CONN_PARAMS_UPDATE_DELAY',['../main_8c.html#adf85796ef07632ed27e0bce9509d9245',1,'main.c']]],
  ['no_5fof_5fpins',['NO_OF_PINS',['../group__app__gpiote.html#ga9ceabb78fed1102abd4f0f331a04e190',1,'app_gpiote.h']]],
  ['no_5fof_5fsamples',['NO_OF_SAMPLES',['../group__adc__module.html#gad4189ffd3a90a1d83afbc51359211d55',1,'bmd_adc.c']]],
  ['nrf_5fadc_5fconfig_5fcustom',['NRF_ADC_CONFIG_CUSTOM',['../group__adc__module.html#ga87712592c89ebeb38cee411e42705ba6',1,'bmd_adc.h']]],
  ['nrf_5fapp_5fpriority_5fhigh',['NRF_APP_PRIORITY_HIGH',['../group__adc__module.html#ga1014e02bb96f2e0e5b1ee927531c80d3',1,'bmd_adc.c']]]
];
