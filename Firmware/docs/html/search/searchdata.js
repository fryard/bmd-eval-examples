var indexSectionsWithContent =
{
  0: "abcdefghilmnoprsuvxyz",
  1: "abg",
  2: "abdimp",
  3: "abimp",
  4: "abcdefghilmoprsuvxyz",
  5: "ab",
  6: "abcdfilmnrs",
  7: "abcdgrv",
  8: "b"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "groups",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros",
  7: "Modules",
  8: "Pages"
};

