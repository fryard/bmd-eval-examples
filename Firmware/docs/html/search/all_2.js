var searchData=
[
  ['conn_5fhandle',['conn_handle',['../structble__bmdeval__s.html#a0d5ffe38d68e48d81e61fc6a4999ae68',1,'ble_bmdeval_s']]],
  ['conn_5fsup_5ftimeout',['CONN_SUP_TIMEOUT',['../main_8c.html#a799412c2b531ca347e13611e6e7523b9',1,'main.c']]],
  ['control_20point_20commands',['Control Point Commands',['../group___control___point___commands.html',1,'']]],
  ['conversion_5fcomplete',['conversion_complete',['../group__adc__module.html#gaca386952daaaf9ba6a7260618a44ce2e',1,'bmd_adc.c']]],
  ['ctrl_5fchar_5fhandles',['ctrl_char_handles',['../structble__bmdeval__s.html#a7fb53c5dbbe8a5a8abb830bc64e339b7',1,'ble_bmdeval_s']]],
  ['ctrl_5fwrite_5fhandler',['ctrl_write_handler',['../structble__bmdeval__init__t.html#a731457f7753fa39fe4d9bf5a7d1e0d84',1,'ble_bmdeval_init_t::ctrl_write_handler()'],['../structble__bmdeval__s.html#a731457f7753fa39fe4d9bf5a7d1e0d84',1,'ble_bmdeval_s::ctrl_write_handler()']]],
  ['current_5fled_5fstate',['current_led_state',['../structble__bmdeval__s.html#aece1fa21b2471578621db492d92283a7',1,'ble_bmdeval_s']]]
];
