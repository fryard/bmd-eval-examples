package com.rigado.bmd200eval.presenters;

public abstract class BasePresenter {

    public abstract void onResume();
    public abstract void onPause();
}
